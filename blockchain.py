# Initialization
genesis_block = {
    'previous_hash': '',
    'index': 0,
    'transactions': []
}
blockchain = [genesis_block]
open_transactions = []
owner = 'Ilo'
participants = {'Edgard'}

########################################################################################
# Blockchain Functions


def hash_block(block):
    return '-'.join([str(block[key]) for key in block])


def get_last_blockchain_value():
    if len(blockchain) < 1:
        return None
    return blockchain[-1]


def get_balance(participant):
    amount_sent = 0
    amount_received = 0
    tx_sender = [[tx['amount'] for tx in block['transactions'] if tx['sender'] == participant] for block in blockchain]
    tx_receiver = [[tx['amount'] for tx in block['transactions'] if tx['receiver'] == participant] for block in blockchain]

    for tx in tx_sender:
        if len(tx) > 0:
            amount_sent += tx[0]

    for tx in tx_receiver:
        if len(tx) > 0:
            amount_received += tx[0]

    return amount_received - amount_sent


def add_transaction(recipient, sender=owner, amount=1.0):
    """ Append  a new value as well as the last blockchain value to the blockchain

    Arguments:
        :sender: The sender of the coins
        :recipient: The recipient of the coins
        :amount: The amount of coins sent with the transaction (default = 1.0)
    """
    transaction = {
        'sender': sender,
        'recipient': recipient,
        'amount': amount
    }
    open_transactions.append(transaction)
    participants.add(sender)
    participants.add(recipient)


def verify_blockchain():
    for idx, block in enumerate(blockchain):
        if idx == 0:
            continue
        if block['previous_hash'] != hash_block(blockchain[idx - 1]):
            return False
    return True


def get_transaction_value():
    tx_recipient = input('Please enter the recipient name: ')
    tx_amount = float(input('Please enter your transaction amount: '))
    return (tx_recipient, tx_amount)


def mine_block():
    last_block = blockchain[-1]
    hashed_block = hash_block(last_block)

    block = {
        'previous_hash': hashed_block,
        'index': len(blockchain),
        'transactions': open_transactions
    }
    blockchain.append(block)
    return True
    # TODO: open_transaction should be reset to empty


def print_blockchain_elements():
    for block in open_transactions:
        print(block)


########################################################################################
# Main Program

wating_for_input = True

while wating_for_input:
    print('Chose an option')
    print('1: Add a new transaction value')
    print('2: Output the blockchain blocks')
    print('h: Manipulate the chain')
    print('q: Quit')

    user_choice = input("Enter your option: ")

    if user_choice == '1':
        tx_data = get_transaction_value()
        recipient, amount = tx_data
        add_transaction(recipient, amount=amount)
        print(open_transactions)
    elif user_choice == '2':
        print_blockchain_elements()
    elif user_choice == 'h':
        if len(blockchain) >= 1:
            blockchain[0] = 2.0
    elif user_choice == 'q':
        wating_for_input = False
    else:
        print('Please chose existed options')
else:
    print('By !')
